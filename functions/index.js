// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const defaultPeriodStats = {
  count: 0,
  paid_learner: 0,
  paid_driver: 0,
}

const getMonth = (date) => {
  date.setDate(1);
  date.setHours(0, 0, 0, 0);
  return date.getTime();
  const months = [date.getTime()];
}

const getWeek = (date) => {
  const diff = date.getDate() - date.getDay();
  date.setDate(diff);
  date.setHours(0, 0, 0, 0);
  return date.getTime();
}

const getDay = (date) => {
  date.setHours(0, 0, 0, 0);
  return date.getTime();
}

exports.updateProfile = functions.auth.user().onCreate(event => {
  const user = event.data; // The Firebase user.


  console.log('>>CREATE USER');
  console.log(user);

  const displayName = user.displayName; // The display name of the user.
  const uid = user.uid; // The display name of the user.
  const phoneNumber = user.phoneNumber; // The display name of the user.
  const activationCode = Math.floor(Math.random() * 9999 - 1000) + 1000


  const updates = {
    [`/learners/${uid}/id`]: uid,
    [`/learners/${uid}/phoneNumber`]: phoneNumber,
    [`/learners/${uid}/activationCode`]: `${activationCode}`,
      [`/learners/${uid}/createdAt`]: `${Date.now()}`,
      [`/learners/${uid}/modifiedAt`]: `${Date.now()}`,
  }

  const day = getDay(new Date())
  const week = getWeek(new Date())
  const month = getMonth(new Date())

  console.log('>>UPDATES');
  console.log(updates);

  admin.database().ref().update(updates)

  admin.database().ref(`stats_daily/${day}`).transaction((statsForDay) => {
    console.log('>> INITIAL STATS DAILTY');
    console.log(statsForDay);
    if (!statsForDay) {
      statsForDay = defaultPeriodStats
    }
    statsForDay.count++

    console.log('>> FINAL STATS DAILY');
    console.log(statsForDay);
    return statsForDay;
  })

  admin.database().ref(`stats_weekly/${week}`).transaction((statsForWeek) => {
    console.log('>> INITIAL STATS WEEKLY');
    console.log(statsForWeek);
    if (!statsForWeek) {
      statsForWeek = defaultPeriodStats
    }
    statsForWeek.count++
      console.log('>> FINAL STATS WEEKLY');
      console.log(statsForWeek);
    return statsForWeek
  })

  admin.database().ref(`stats_monthly/${month}`).transaction((statsForMonth) => {
    console.log('>> INITIAL STATS MONTHLY');
    console.log(statsForMonth);
    if (!statsForMonth) {
      statsForMonth = defaultPeriodStats
    }
    statsForMonth.count++
    console.log('>> FINAL STATS MONTHLY');
    console.log(statsForMonth);
    return statsForMonth
  })

  admin.database().ref('meta_learners').transaction((learnersMeta) => {
    console.log('>> INITIAL META LEARNER');
    console.log(learnersMeta);
    if (!learnersMeta) {
      learnersMeta = defaultPeriodStats
    }
    learnersMeta.count++
    console.log('>> FINAL META LEARNER');
    console.log(learnersMeta);
    return learnersMeta
  })
})

exports.deleteProfile = functions.auth.user().onDelete(event => {
  const user = event.data; // The Firebase user.
  const uid = user.uid; // The uid of the user.
  // admin.database().ref(`/learners/${uid}`).set(null)
})

exports.copyProfileToFireStore = functions.database.ref('/learners/{userId}')
  .onWrite(event => {
    // Grab the current value of what was written to the Realtime Database.
    const original = event.data.val();
    console.log('Updating Customer', event.params.userId, original);
    return admin.firestore().collection("learners").doc(event.params.userId).set(original);
})

exports.tester = functions.https.onRequest((req, res) => {
    // Grab the text parameter.
    return res.json(200, {'it': 'is working'});
});

const getListSection = (ref, startAt) => {
    let query = ref;

    if (startAt) {
        query = query.startAt(startAt);
    }

    // return Promise.resolve({getListSectionStartAt: startAt});

    return query.once("value").then((dataSnapshot) => {
        return dataSnapshot;
    });
}

const getBaseQuery = (ref, limit = 5, page = 1) => {
    let query = ref.orderByKey();

    if (limit) {
        query = query.limitToFirst(limit)
    }

    return query;
}

const getList = (ref, limit = 5, page = 1) => {
    let query = getBaseQuery(ref, limit, page);
    let currentPage = 1;
    let startAt = null;

    return getListSection(query, null).then((dataSnapshot) => {
        if (!dataSnapshot.exists() || currentPage >= page) {
            return dataSnapshot.toJSON();
        }

        dataSnapshot.forEach((childDataSnapShot) => {
            startAt = childDataSnapShot.key;
        });

        query = getBaseQuery(ref, limit, page);
        // do some stuff once
        return getListSection(query, startAt).toJSON();
    });
}

exports.clients = functions.https.onRequest((req, res) => {
    /*
    response.json(200, {
                  order: null,
                  error: e.message
              })
     */
    // Grab the text parameter.
    let page = req.query.page;
    let limit = req.query.limit;
// Push the new message into the Realtime Database using the Firebase Admin SDK.

    if (page) {
        page = parseInt(page)
        if (isNaN(page)) {
            page = 1;
        }
    } else {
        page = 1;
    }

    if (limit) {
        limit = parseInt(limit)
        if (isNaN(limit)) {
            limit = 10;
        }
    } else {
        limit = 10;
    }

    const db = admin.database();
    const ref = db.ref("/learners");

    return getList(ref, limit, page).then((list) => {
        return res.json(200, {list});
    })
});
